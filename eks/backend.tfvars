bucket                      = "stephan-rudolph-devops-demo.tfstate"
key                         = "environment/argocdinfra.json"
skip_region_validation      = true
region                      = "eu-central-2"
workspace_key_prefix        = "environment"
dynamodb_table              = "stephan-rudolph-devops-demo.tfstate.lock"
