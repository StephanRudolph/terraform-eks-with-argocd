// get the remote state data for eks
data "terraform_remote_state" "eks" {
  backend = "s3"
  workspace = terraform.workspace
  config = {
    bucket                      = "stephan-rudolph-devops-demo.tfstate"
    key                         = "environment/argocdinfra.json"
    skip_region_validation      = true
    region                      = "eu-central-2"
    workspace_key_prefix        = "environment"
    dynamodb_table              = "stephan-rudolph-devops-demo.tfstate.lock"
  }
}
